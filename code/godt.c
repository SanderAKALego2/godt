/*

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

TL;DR

Use it however you want, I don't care, but dont come complaining to me if it
bricks your computer.



Created by Sander Kvernstuen Persen 12017 (HE) (2017 AD)

*/

#define STB_IMAGE_IMPLEMENTATION
#define UNICODE

#include <Windows.h>
#include <Windowsx.h>
#include <Commctrl.h>
#include <dsound.h>
#include "sp_defines.c"
#include "sp_types.c"
#include "stb_image.h"

#define S(x) #x
#define S_(x) S(x)
#define S__LINE__ S_(__LINE__)
#ifdef Assert
#undef Assert
#define Assert(Expr) if(!(Expr)) { MessageBoxA(0,"File: "__FILE__"\n""Line: "S__LINE__"\n"#Expr,"---ASSERTION FAILED---",MB_ICONERROR|MB_OKCANCEL); *(int *)0 = 0;}
#endif
#define MaxCounters 100

#define VERSION L"THIS IS AN EXPERIMENTAL BUILD, EXPECT THINGS NOT TO WORK\n\nVersion is: 0.0.0.7\nLast Build: "__DATE__
enum Identifications
{
    ID_FIRST_COUNTER_ID = 1,
    ID_LAST_COUNTER_ID = MaxCounters,
    ID_EXIT,   
    ID_SAVE,   
    ID_TIMER,  
    ID_DATE_EDIT, 
    ID_VER,       
    ID_PLAY_SOUND, 
    ID_HIDE, 
    ID_TOM_TAKEOVER,
    ID_DANCE,
};

#define DEFAULT_VALUE (1 << 31)

#define DSoundBufferSamplesPerSecond 48000

typedef struct app_config_counter
{
    u32 ID;
    s32 x;
    s32 y;
    wchar_t *Label;
    wchar_t *ImgUp;
    wchar_t *ImgDown;
    wchar_t *ImgHiddenUp;
    wchar_t *ImgHiddenDown;
    wchar_t *SoundUp;
    wchar_t *SoundDown;
    wchar_t *SoundMilestone;
}app_config_counter;

typedef struct app_config
{
    u32 AllocatedCounterCount;
    u32 CounterCount;
    s32 WindowWidth;
    s32 WindowHeight;
    s32 UpDownX;
    s32 UpDownY;
    s32 UpDownW;
    s32 UpDownH;
    s32 DateX;
    s32 DateY;
    s32 DateW;
    s32 DateH;
    s32 TimerModMS;
    wchar_t *DefaultImage;
    wchar_t *DefaultHiddenImage;
    wchar_t *ErikRegular;
    wchar_t *ErikSmor;
    wchar_t *ErikSurprised;
    wchar_t *Book;
    wchar_t *AngryTom;
    app_config_counter *Counters;
}app_config;

typedef struct win32_sound_output
{
    int SamplesPerSecond;
    u32 RunningSampleIndex;
    int BytesPerSample;
    DWORD SecondaryBufferSize;
    DWORD SafetyBytes;
}win32_sound_output;

typedef struct game_sound_output_buffer
{
    int SamplesPerSecond;
    int SampleCount;
    
    // IMPORTANT(casey): Samples must be padded to a multiple of 4 samples!
    s16 *Samples;
}game_sound_output_buffer;

typedef struct audio_clip
{
    b32 IsValid;
    int SampleCount;
    int Channels;
    int SamplesPerSecond;
    int BytesPerSample;
    s16 *Samples;
    r32 LengthInSeconds;
    int BufferSize;
    LPDIRECTSOUNDBUFFER Buffer;
}audio_clip;

typedef struct win32_bitmap
{
    wchar_t *Name;
    LPVOID Pixels;
    int Width;
    int Height;
    HBITMAP BitmapHandle;
}win32_bitmap;

typedef struct win32_read_file
{
    u64 Size;
    void *Content;
}win32_read_file;

typedef struct counter_handles
{
    b32 Hidden;
    HWND Text;
    HWND UpDown;
    HWND Buddy;
}counter_handles;

typedef struct counter
{
    b32 Used;
    u32 ID;
    counter_handles *Handles;
    win32_bitmap BitmapUp;
    win32_bitmap BitmapDown;
    win32_bitmap BitmapHiddenUp;
    win32_bitmap BitmapHiddenDown;
    s32 PrevValue;
    audio_clip SoundUp;
    audio_clip SoundDown;
    audio_clip SoundMilestone;
}counter;

typedef struct stat_line
{
    u32 FieldCount;
    wchar_t *Label;
    u32 *Fields;
}stat_line;

struct
{
    // NOTE: 'Godt', 'Greit' osv
    u32 FieldNameCount;
    u32 StatCount;
    wchar_t **FieldNames;
    stat_line *Stats;
}GlobalStatsNew;

enum Hosts
{
    Tom,
    Erik,
    Bjorn,
    Ivar
};

global HWND MainWindow;

global u32 Host;

global HWND DateText;
global HMENU TopMenu;
global HMENU ContextMenu;

global win32_bitmap DefaultBitmap;
global win32_bitmap DefaultHiddenBitmap;
global win32_bitmap ErikRegularBitmap;
global win32_bitmap ErikSmorBitmap;
global win32_bitmap ErikSurprisedBitmap;
global win32_bitmap BookBitmap;
global win32_bitmap AngryTomBitmap;
global win32_bitmap GlobalBackBuffer;

global u64 LastWriteTime;

global u32 GlobalCounterCount;
global counter *GlobalCounters;

global LPDIRECTSOUND GlobalDirectSound;
global audio_clip GlobalGodtSound;
global audio_clip GlobalSadSound;
global audio_clip GlobalDanceSound;;

global b32 IsPlayingSound;

global b32 GlobalHiddenMode;

enum RenderPushTypes
{
    RenderPushBitmap
};

typedef struct push_bitmap
{
    win32_bitmap *Bitmap;
    s32 x,y,w,h;
}push_bitmap;

typedef struct push_command
{
    u32 Type;
    u32 Size;
    u8 Data[];
}push_command;

struct
{
    u32 Count;
    u8 *Buffer;
    u8 *At;
    u8 *End;
    HANDLE Mutex;
}RenderCommands;

//global win32_bitmap *BitmapToDraw = &DefaultBitmap;

inline s32
GetCounterValue(u32 Index)
{
    wchar_t Buffer[32];
    GetWindowText(GlobalCounters[Index].Handles->Buddy, Buffer, ArrayCount(Buffer));
    s32 Result = _wtoi(Buffer);
    return(Result);
}

inline void
MemCopy(void *Dest, void *Source, umm Size)
{
    u8 *D = (u8*)Dest;
    u8 *S = (u8*)Source;
    for(u32 Byte = 0;
        Byte < Size;
        ++Byte)
    {
        *D++ = *S++;
    }
}

internal void*
Win32_Allocate(umm Size)
{
    void *Memory = VirtualAlloc(0, Size, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
    return(Memory);
}

internal void
Win32_Deallocate(void *Memory)
{
    if(Memory)
    {
        VirtualFree(Memory, 0, MEM_DECOMMIT);
    }
}

internal void*
Win32_Reallocate(void *OldMemory, umm OldSize, umm NewSize)
{
    void *Temp = Win32_Allocate(OldSize);
    if(Temp)
    {
        MemCopy(Temp, OldMemory, OldSize);
    }
    Win32_Deallocate(OldMemory);
    void *Result = Win32_Allocate(NewSize);
    if(Temp)
    {
        MemCopy(Result, Temp, OldSize);
    }
    return(Result);
}

inline wchar_t*
ReadLine(wchar_t **FilePointer)
{
    wchar_t *LineBegin = *FilePointer;
    if(*LineBegin)
    {
        while(**FilePointer == '\r' || **FilePointer == '\n')
        {
            (*FilePointer)++;
            LineBegin = *FilePointer;
        }
        do
        {
            if(**FilePointer == '\r' || **FilePointer == '\n')
            {
                **FilePointer = 0;
                (*FilePointer)++;
                if(**FilePointer == '\n')
                {
                    **FilePointer = 0;
                    (*FilePointer)++;
                }
                break;
            }
        }while(*(*FilePointer)++);
    }
    else
    {
        LineBegin = 0;
    }
    return(LineBegin);
}

inline wchar_t**
SplitString(wchar_t *String, wchar_t Char, u32 *CountOut)
{
    wchar_t **Result = 0;
    *CountOut = 1;
    wchar_t *Scan = String;
    do
    {
        if(*Scan == Char)
        {
            *CountOut += 1;
        }
    }while(*Scan++);
    Scan = String;
    Result = Win32_Allocate(sizeof(wchar_t) * *CountOut);
    for(u32 StringIndex = 0;
        StringIndex < *CountOut;
        ++StringIndex)
    {
        Result[StringIndex] = Scan;
        do
        {
            if(*Scan == Char)
            {
                *Scan++ = 0;
                break;
            }
        }while(*Scan++);
    }
    return(Result);
}

inline u32
StringLength(wchar_t *Str)
{
    u32 Len = 0;
    while(*Str++) ++Len;
    return(Len);
}

inline wchar_t*
StringCopyN(wchar_t *Source, umm Length)
{
    wchar_t *Result = Win32_Allocate((Length) * sizeof(wchar_t));
    wchar_t *Scan = Result;
    do
    {
        *Scan++ = *Source++;
    }while(*Source);
    *Scan = 0;
    return(Result);
}

inline wchar_t*
StringCopy(wchar_t *Source)
{
    u32 Length = StringLength(Source);
    return(StringCopyN(Source, Length));
}

inline b32
StringContains(wchar_t *Buffer, wchar_t Char)
{
    b32 Result = false;
    do
    {
        if(*Buffer == Char)
        {
            Result = true;
            break;
        }
    }while(*Buffer++);
    return(Result);
}

inline b32
StringsAreEqual(wchar_t *Str1, wchar_t *Str2)
{
    return(!wcscmp(Str1, Str2));
    /*b32 Result = true;
    do
    {
        if(*A != *B)
        {
            Result = false;
            break;
        }
    }while((*A)++ && (*B)++);
    return(Result);*/
}

inline b32
WhiteSpace(wchar_t Char)
{
    return(Char == ' ' || Char == '\n' || Char == '\t');
}

inline wchar_t*
Trim(wchar_t *String)
{
    wchar_t *BeginScan = String;
    wchar_t *EndScan = String + StringLength(String);
    while(WhiteSpace(*BeginScan))
    {
        BeginScan++;
    }
    while(WhiteSpace(*EndScan))
    {
        *EndScan++ = 0;
    }
    return(BeginScan);
}

internal void
HideCounter(counter *Counter, b32 Hide)
{
    Counter->Handles->Hidden = Hide;
    if(Counter->Handles)
    {
#if 0
        if(Hide)
        {
            OutputDebugString(L"Counter hidden\n");
        }
        else
        {
            OutputDebugString(L"Counter shown\n");
        }
#endif
        ShowWindow(Counter->Handles->UpDown, !Hide);
        ShowWindow(Counter->Handles->Buddy, !Hide);
        ShowWindow(Counter->Handles->Text, !Hide);
    }
    else
    {
        Assert(!"Counter not created properly");
    }
}

internal void
DestroyCounter(counter *Counter)
{
    if(Counter->Handles)
    {
        Assert(Counter->Handles->Text);
        Assert(Counter->Handles->UpDown);
        Assert(Counter->Handles->Buddy);
        DestroyWindow(Counter->Handles->Text);
        DestroyWindow(Counter->Handles->UpDown);
        DestroyWindow(Counter->Handles->Buddy);
        Win32_Deallocate(Counter->Handles);
        Counter->Handles = 0;
    }
    Counter->Handles->Text = 0;
    Counter->Handles->UpDown = 0;
    Counter->Handles->Buddy = 0;
}

inline void
CopyPixels(u8 *Dest, u8 *Source, s32 Width, s32 Height)
{
    for(int y = 0; y < Height; ++y)
    {
        for(int x = 0; x < Width; ++x)
        {
            u32 Offset = x + y * Width;
            u8 *PixelPtr = (u8*)((u32*)Source + Offset);
            u8 R = *(PixelPtr);
            u8 G = *(PixelPtr+1);
            u8 B = *(PixelPtr+2);
            u8 A = *(PixelPtr+3);
            u32 Pixel = B | G << 8 | R << 16 | A << 24;
            *((u32*)Dest + Offset) = Pixel;
        }
    }
}

internal u64
Win32_GetFileSize(wchar_t *Filepath)
{
    u64 FileSize = 0;
    HANDLE FileHandle = CreateFile(Filepath, 
                                   GENERIC_READ, 
                                   FILE_SHARE_READ|FILE_SHARE_WRITE,
                                   0, OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,0);
    if(FileHandle)
    {
        LARGE_INTEGER Size;
        if(GetFileSizeEx(FileHandle, &Size))
        {
            FileSize = Size.QuadPart;
        }
        CloseHandle(FileHandle);
    }
    return(FileSize);
}

internal win32_read_file
Win32_ReadEntireFile(wchar_t *Path)
{
    win32_read_file Win32ReadFile = {0};
    HANDLE FileHandle = CreateFile(Path, 
                                   GENERIC_READ, 
                                   FILE_SHARE_READ|FILE_SHARE_WRITE,
                                   0, OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,0);
    if(FileHandle != INVALID_HANDLE_VALUE)
    {
        LARGE_INTEGER FileSize = {0};
        if(GetFileSizeEx(FileHandle, &FileSize))
        {
            Win32ReadFile.Size = FileSize.QuadPart;
            if(Win32ReadFile.Size)
            {
                Win32ReadFile.Content = Win32_Allocate(FileSize.QuadPart);
                if(Win32ReadFile.Content)
                {
                    DWORD BytesRead;
                    Assert(Win32ReadFile.Size < 0xffffffff);
                    if(ReadFile(FileHandle, Win32ReadFile.Content, Win32ReadFile.Size, &BytesRead, 0) && BytesRead == Win32ReadFile.Size)
                    {
                        
                    }
                    else
                    {
                        Win32_Deallocate(Win32ReadFile.Content);
                        Win32ReadFile.Content = 0;
                    }
                }
            }
            else
            {
                // TODO: Logging...
            }
        }
        else
        {
            // TODO: Logging...
        }
        CloseHandle(FileHandle);
    }
    else
    {
        // TODO: Logging...
    }
    return(Win32ReadFile);
}

internal HBITMAP
Win32_CreateBitmap(s32 Width, s32 Height, void **Pixels, int BitCount)
{
    HBITMAP BitmapHandle = 0;
    HDC DeviceContextScreen = GetDC(0);
    if(DeviceContextScreen)
    {
        HDC DeviceContextBitmap = CreateCompatibleDC(DeviceContextScreen);
        if(DeviceContextBitmap)
        {
            BITMAPINFO BitmapInfo = {0};
            BitmapInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
            BitmapInfo.bmiHeader.biWidth = Width;
            BitmapInfo.bmiHeader.biHeight = -Height;
            BitmapInfo.bmiHeader.biPlanes = 1;
            BitmapInfo.bmiHeader.biBitCount = BitCount;
            BitmapInfo.bmiHeader.biCompression = BI_RGB;
            BitmapHandle = CreateDIBSection(DeviceContextScreen, 
                                            &BitmapInfo,
                                            DIB_RGB_COLORS, Pixels, 0, 0);
            if(ReleaseDC(0, DeviceContextScreen))
            {
            }
            else
            {
                // TODO: Logging ...
            }
        }
        else
        {
            // TODO: Logging ...
        }
    }
    else
    {
        // TODO: Logging ...
    }
    return(BitmapHandle);
}

inline wchar_t*
ExtractFileName(wchar_t *FilePath)
{
    wchar_t *Result = FilePath;
    wchar_t *Scan = FilePath;
    do
    {
        if(*Scan == '\\' || *Scan == '/')
        {
            Result = Scan;
        }
    }while(*Scan++);
    return(Result);
}

internal win32_bitmap
Win32_LoadBitmap(wchar_t *Path)
{
    win32_bitmap Win32Bitmap = {0};
    win32_read_file Win32ReadFile = Win32_ReadEntireFile(Path);
    if(Win32ReadFile.Content)
    {
        u8 *ImagePixels = stbi_load_from_memory(Win32ReadFile.Content,
                                                Win32ReadFile.Size,
                                                &Win32Bitmap.Width, 
                                                &Win32Bitmap.Height, 
                                                0, 4);
        if(ImagePixels)
        {
            Win32Bitmap.BitmapHandle = Win32_CreateBitmap(Win32Bitmap.Width,
                                                          Win32Bitmap.Height,
                                                          &Win32Bitmap.Pixels,
                                                          32);
            if(Win32Bitmap.BitmapHandle)
            {
                CopyPixels(Win32Bitmap.Pixels, ImagePixels, Win32Bitmap.Width, Win32Bitmap.Height);
                Win32Bitmap.Name = StringCopy(ExtractFileName(Path));
            }
        }
        else
        {
            // TODO: Logging
        }
        stbi_image_free(ImagePixels);
    }
    else
    {
        // TODO: Logging
    }
    Win32_Deallocate(Win32ReadFile.Content);
    
    return(Win32Bitmap);
}

internal void
Win32_DeleteBitmap(HBITMAP BitmapHandle)
{
    DeleteObject(BitmapHandle);
}

internal void
Win32_FreeBitmap(win32_bitmap *Bitmap)
{
    if(Bitmap->Name)
    {
        Win32_Deallocate(Bitmap->Name);
    }
    if(Bitmap->BitmapHandle)
    {
        DeleteObject(Bitmap->BitmapHandle);
    }
    Bitmap->Name = 0;
    Bitmap->Pixels = 0;
    Bitmap->Width = 0;
    Bitmap->Height = 0;
    Bitmap->BitmapHandle = 0;
}

internal void
Win32_DrawBitmap(win32_bitmap *Bitmap, int x, int y, int Width, int Height, HDC DeviceContext)
{
    HDC DeviceContextScreen = GetDC(0);
    if(DeviceContextScreen)
    {
        HDC DeviceContextSource = CreateCompatibleDC(DeviceContextScreen);
        if(DeviceContextSource)
        {
            HBITMAP BitmapOld = SelectObject(DeviceContextSource, Bitmap->BitmapHandle);
            if(BitmapOld)
            {
                int ActualWidth = (Width == 0 ? Bitmap->Width : Width);
                int ActualHeight = (Height == 0 ? Bitmap->Height : Height);
                
                //if(BitBlt(DeviceContext, x, y, ActualWidth, ActualHeight, DeviceContextSource, 0, 0, SRCCOPY))
                if(StretchBlt(DeviceContext,
                              x,y,
                              ActualWidth,
                              ActualHeight,
                              DeviceContextSource,
                              0,0,
                              Bitmap->Width,
                              Bitmap->Height,
                              SRCCOPY))
                {
                    if(SelectObject(DeviceContextSource, BitmapOld))
                    {
                        if(DeleteDC(DeviceContextSource))
                        {
                        }
                        else
                        {
                            // TODO: Logging...
                        }
                    }
                    else
                    {
                        // TODO: Logging...
                    }
                }
                else
                {
                    // TODO: Logging...
                }
            }
            else
            {
                // TODO: Logging...
            }
        }
        else
        {
            // TODO: Logging...
        }
    }
    else
    {
        // TODO: Logging...
    }
}

#define DIRECT_SOUND_CREATE(name) HRESULT WINAPI name(LPCGUID pcGuidDevice, LPDIRECTSOUND *ppDS, LPUNKNOWN pUnkOuter)
typedef DIRECT_SOUND_CREATE(direct_sound_create);

internal void
Win32_InitDSound(HWND Window, s32 SamplesPerSecond)
{
    HMODULE DSoundLibrary = LoadLibraryW(L"dsound.dll");
    if(DSoundLibrary)
    {
        direct_sound_create *DirectSoundCreate = (direct_sound_create *)
            GetProcAddress(DSoundLibrary, "DirectSoundCreate");
        
        if(DirectSoundCreate && SUCCEEDED(DirectSoundCreate(0, &GlobalDirectSound, 0)))
        {
            WAVEFORMATEX WaveFormat = {0};
            WaveFormat.wFormatTag = WAVE_FORMAT_PCM;
            WaveFormat.nChannels = 2;
            WaveFormat.nSamplesPerSec = SamplesPerSecond;
            WaveFormat.wBitsPerSample = 16;
            WaveFormat.nBlockAlign = (WaveFormat.nChannels*WaveFormat.wBitsPerSample) / 8;
            WaveFormat.nAvgBytesPerSec = WaveFormat.nSamplesPerSec*WaveFormat.nBlockAlign;
            WaveFormat.cbSize = 0;
            
            if(SUCCEEDED(IDirectSound8_SetCooperativeLevel(GlobalDirectSound, 
                                                           Window, 
                                                           DSSCL_PRIORITY)))
            {
                DSBUFFERDESC BufferDescription = {0};
                BufferDescription.dwSize = sizeof(BufferDescription);
                BufferDescription.dwFlags = DSBCAPS_PRIMARYBUFFER;
                
                LPDIRECTSOUNDBUFFER PrimaryBuffer;
                if(SUCCEEDED(GlobalDirectSound->lpVtbl->CreateSoundBuffer(GlobalDirectSound, 
                                                                          &BufferDescription, 
                                                                          &PrimaryBuffer, 0)))
                {
                    HRESULT Error = PrimaryBuffer->lpVtbl->SetFormat(PrimaryBuffer, 
                                                                     &WaveFormat);
                    if(SUCCEEDED(Error))
                    {
                        OutputDebugStringA("Primary buffer format was set.\n");
                    }
                    else
                    {
                        // TODO(casey): Diagnostic
                    }
                }
                else
                {
                    // TODO(casey): Diagnostic
                }
            }
            else
            {
                // TODO(casey): Diagnostic
            }
        }
        else
        {
            // TODO(casey): Diagnostic
        }
    }
    else
    {
        // TODO(casey): Diagnostic
    }
}

internal void
Win32_ClearBuffer(audio_clip *AudioClip)
{
    VOID *Region1;
    DWORD Region1Size;
    VOID *Region2;
    DWORD Region2Size;
    if(SUCCEEDED(AudioClip->Buffer->lpVtbl->Lock(AudioClip->Buffer, 0, 
                                                 AudioClip->BufferSize,
                                                 &Region1, &Region1Size,
                                                 &Region2, &Region2Size,
                                                 0)))
    {
        // TODO(casey): assert that Region1Size/Region2Size is valid
        u8 *DestSample = (u8*)Region1;
        for(DWORD ByteIndex = 0;
            ByteIndex < Region1Size;
            ++ByteIndex)
        {
            *DestSample++ = 0;
        }
        
        DestSample = (u8*)Region2;
        for(DWORD ByteIndex = 0;
            ByteIndex < Region2Size;
            ++ByteIndex)
        {
            *DestSample++ = 0;
        }
        
        AudioClip->Buffer->lpVtbl->Unlock(AudioClip->Buffer, 
                                          Region1, Region1Size, Region2, Region2Size);
    }
}

internal void
Win32_FillSoundBuffer(audio_clip *AudioClip)
{
    // TODO(casey): More strenuous test!
    VOID *Region1;
    DWORD Region1Size;
    VOID *Region2;
    DWORD Region2Size;
    if(SUCCEEDED(AudioClip->Buffer->lpVtbl->Lock(AudioClip->Buffer, 
                                                 0, AudioClip->BufferSize,
                                                 &Region1, &Region1Size,
                                                 &Region2, &Region2Size,
                                                 0)))
    {
        // TODO(casey): assert that Region1Size/Region2Size is valid
        
        // TODO(casey): Collapse these two loops
        DWORD Region1SampleCount = Region1Size/AudioClip->BytesPerSample;
        s16 *DestSample = (s16*)Region1;
        s16 *SourceSample = AudioClip->Samples;
        for(DWORD SampleIndex = 0;
            SampleIndex < Region1SampleCount;
            ++SampleIndex)
        {
            *DestSample++ = *SourceSample++;
            *DestSample++ = *SourceSample++;
        }
        
        DWORD Region2SampleCount = Region2Size/AudioClip->BytesPerSample;
        DestSample = (s16*)Region2;
        for(DWORD SampleIndex = 0;
            SampleIndex < Region2SampleCount;
            ++SampleIndex)
        {
            *DestSample++ = *SourceSample++;
            *DestSample++ = *SourceSample++;
        }
        
        AudioClip->Buffer->lpVtbl->Unlock(AudioClip->Buffer, 
                                          Region1, Region1Size, Region2, Region2Size);
    }
}

internal b32
CreateSoundBuffer(audio_clip *AudioClip)
{
    b32 Result = false;
    WAVEFORMATEX WaveFormat = {0};
    WaveFormat.wFormatTag = WAVE_FORMAT_PCM;
    WaveFormat.nChannels = 2;
    WaveFormat.nSamplesPerSec = AudioClip->SamplesPerSecond;
    WaveFormat.wBitsPerSample = 16;
    WaveFormat.nBlockAlign = (WaveFormat.nChannels*WaveFormat.wBitsPerSample) / 8;
    WaveFormat.nAvgBytesPerSec = WaveFormat.nSamplesPerSec*WaveFormat.nBlockAlign;
    WaveFormat.cbSize = 0;
    
    DSBUFFERDESC BufferDescription = {0};
    BufferDescription.dwSize = sizeof(BufferDescription);
    BufferDescription.dwFlags = DSBCAPS_GETCURRENTPOSITION2;
#if 1
    BufferDescription.dwFlags |= DSBCAPS_GLOBALFOCUS;
#endif
    BufferDescription.dwBufferBytes = AudioClip->BufferSize;
    BufferDescription.lpwfxFormat = &WaveFormat;
    HRESULT Error = GlobalDirectSound->lpVtbl->CreateSoundBuffer(GlobalDirectSound, 
                                                                 &BufferDescription, 
                                                                 &AudioClip->Buffer, 0);
    if(SUCCEEDED(Error))
    {
        Result = true;
    }
    return(Result);
}

internal audio_clip
LoadAudioClip(wchar_t *Path)
{
    audio_clip AudioClip = {0};
    win32_read_file FileRead = Win32_ReadEntireFile(Path);
    if(FileRead.Content)
    {
        AudioClip.SampleCount = stb_vorbis_decode_memory(FileRead.Content, 
                                                         FileRead.Size, 
                                                         &AudioClip.Channels, 
                                                         &AudioClip.SamplesPerSecond, 
                                                         &AudioClip.Samples);
        AudioClip.LengthInSeconds = ((r32)AudioClip.SampleCount / (r32)AudioClip.SamplesPerSecond);
        AudioClip.BytesPerSample = sizeof(s16)*2;
        AudioClip.BufferSize = AudioClip.SampleCount * AudioClip.BytesPerSample;
        
        if(CreateSoundBuffer(&AudioClip))
        {
            Win32_ClearBuffer(&AudioClip);
            Win32_Deallocate(FileRead.Content);
            AudioClip.IsValid = true;
        }
    }
    return(AudioClip);
}

internal counter
CreateCounter(wchar_t *Label, 
              wchar_t *ImageUp, 
              wchar_t *ImageDown, 
              wchar_t *ImageHiddenUp, 
              wchar_t *ImageHiddenDown, 
              wchar_t *SoundUp, 
              wchar_t *SoundDown,
              wchar_t *SoundMilestone,
              s32 x, s32 y, s32 Width, s32 Height,  u32 Min, u32 Max, HWND Parent, HINSTANCE hInstance, u32 ID)
{
    Assert(ID >= ID_FIRST_COUNTER_ID && ID <= ID_LAST_COUNTER_ID);
    counter Counter = 
    {
        .ID = ID,
        .Handles = Win32_Allocate(sizeof(counter_handles)),
    };
    if(ImageUp)
    {
        Counter.BitmapUp = Win32_LoadBitmap(ImageUp);
        if(StringsAreEqual(ImageUp, ImageDown))
        {
            Counter.BitmapDown = Counter.BitmapUp;
        }
    }
    if(ImageDown)
    {
        Counter.BitmapDown = Win32_LoadBitmap(ImageDown);
    }
    if(ImageHiddenUp)
    {
        Counter.BitmapHiddenUp = Win32_LoadBitmap(ImageHiddenUp);
        if(StringsAreEqual(ImageHiddenUp, ImageHiddenDown))
        {
            Counter.BitmapHiddenDown = Counter.BitmapHiddenUp;
        }
    }
    if(ImageHiddenDown)
    {
        Counter.BitmapHiddenDown = Win32_LoadBitmap(ImageHiddenDown);
    }
    if(SoundUp)
    {
        Counter.SoundUp = LoadAudioClip(SoundUp);
        if(StringsAreEqual(SoundUp, SoundDown))
        {
            Counter.SoundDown = Counter.SoundUp;
        }
    }
    if(SoundDown)
    {
        Counter.SoundDown = LoadAudioClip(SoundDown);
    }
    if(SoundMilestone)
    {
        Counter.SoundMilestone = LoadAudioClip(SoundMilestone);
    }
    Assert(Counter.Handles);
    Counter.Handles->Text = CreateWindowEx(0,
                                           L"STATIC",
                                           0,WS_CHILD|WS_VISIBLE|SS_CENTER,
                                           x,y,Width,Height,Parent,0,
                                           hInstance, 0);
    Counter.Handles->UpDown = CreateWindow(UPDOWN_CLASSW, 
                                           0, 
                                           WS_CHILD|WS_VISIBLE|UDS_SETBUDDYINT|UDS_ALIGNRIGHT|UDS_ARROWKEYS , 
                                           x + Width, 
                                           y + 20, 15, Height,
                                           Parent, 
                                           0, 0, 0);
    Counter.Handles->Buddy = CreateWindowExW(WS_EX_CLIENTEDGE,
                                             WC_EDITW, 
                                             L"0", 
                                             WS_CHILD 
                                             | WS_VISIBLE | ES_RIGHT, 
                                             x, y + 20, Width, Height, 
                                             Parent, (HMENU)Counter.ID, 0, 0);
    if(Counter.Handles->UpDown && Counter.Handles->Buddy)
    {
        SendMessage(Counter.Handles->UpDown, UDM_SETBUDDY, (WPARAM)Counter.Handles->Buddy, 0);
        SendMessage(Counter.Handles->UpDown, UDM_SETRANGE, 0, MAKELPARAM(Max, Min));
    }
    else
    {
        // TODO: Logging ...
    }
    HideCounter(&Counter, false);
    return(Counter);
}

inline s64
ParseValueInt(wchar_t *Value)
{
    s64 Result = DEFAULT_VALUE;
    if(!StringsAreEqual(Value, L"default"))
    {
        Result = _wtoi(Value);
    }
    return(Result);
}

inline b32
StartsWith(wchar_t *Str, wchar_t *Start)
{
    b32 Result = true;
    while(*Str && *Start)
    {
        if(*Str != *Start)
        {
            Result = false;
            break;
        }
        Str++;
        Start++;
    }
    return(Result);
}

internal app_config
ReadConfig(wchar_t *Filepath)
{
    app_config AppConfig = 
    {
        .WindowWidth = DEFAULT_VALUE,
        .WindowHeight = DEFAULT_VALUE,
        .UpDownX = 70,
        .UpDownY = 155,
        .UpDownW = 75,
        .UpDownH = 20,
        .DateX = 70,
        .DateY = 177,
        .DateW = 75,
        .DateH = 20,
        .DefaultImage = L"img0.png",
        .TimerModMS = 120000,
    };
    win32_read_file ReadFile = Win32_ReadEntireFile(Filepath);
    if(ReadFile.Content)
    {
        wchar_t *Buffer = 0;
        s32 BufferSize =MultiByteToWideChar(CP_UTF8,0,ReadFile.Content,ReadFile.Size,Buffer,0);
        Buffer = Win32_Allocate(sizeof(wchar_t) * (BufferSize + 1));
        MultiByteToWideChar(CP_UTF8,0,ReadFile.Content,ReadFile.Size,Buffer,BufferSize);
        wchar_t *FilePointer = Buffer;
        wchar_t *Line = 0;
        while((Line = ReadLine(&FilePointer)))
        {
            Line = Trim(Line);
            if(Line[0] != '#')
            {
                u32 FieldCount = 0;
                wchar_t **Fields = SplitString(Line, ':', &FieldCount);
                Assert(FieldCount == 2);
                wchar_t *Type = Trim(Fields[0]);
                wchar_t *Value = Trim(Fields[1]);
                b32 FoundConfig = true;
                if(StartsWith(Type, L"counter"))
                {
                    u32 SplitCount = 0;
                    wchar_t **CounterSplit = SplitString(Type, '_', &SplitCount);
                    // NOTE: Counter IDs start at 1 (0 is not a valid ID)
                    s32 ID  = (_wtoi(CounterSplit[1])) + 1;
                    s32 FoundIndex = -1;
                    for(u32 CounterIndex = 0;
                        CounterIndex < AppConfig.CounterCount;
                        ++CounterIndex)
                    {
                        if(AppConfig.Counters[CounterIndex].ID == ID)
                        {
                            FoundIndex = CounterIndex;
                        }
                    }
                    if(FoundIndex == -1)
                    {
                        u32 OldSize = sizeof(app_config_counter) * AppConfig.CounterCount;
                        AppConfig.CounterCount++;
                        u32 NewSize = sizeof(app_config_counter) * AppConfig.CounterCount;
                        AppConfig.Counters = Win32_Reallocate(AppConfig.Counters, OldSize, NewSize);
                        FoundIndex = AppConfig.CounterCount - 1;
                    }
                    s32 Index = FoundIndex;
                    AppConfig.Counters[Index].ID = ID;
                    if(StringsAreEqual(CounterSplit[2], L"x"))
                    {
                        AppConfig.Counters[Index].x = ParseValueInt(Value);
                    }
                    else if(StringsAreEqual(CounterSplit[2], L"y"))
                    {
                        AppConfig.Counters[Index].y = ParseValueInt(Value);
                    }
                    else if(StringsAreEqual(CounterSplit[2], L"label"))
                    {
                        AppConfig.Counters[Index].Label = StringCopy(Value);
                    }
                    else if(StringsAreEqual(CounterSplit[2], L"img"))
                    {
                        AppConfig.Counters[Index].ImgUp = StringCopy(Value);
                        AppConfig.Counters[Index].ImgDown = AppConfig.Counters[Index].ImgUp;
                    }
                    else if(StringsAreEqual(CounterSplit[2], L"imgup"))
                    {
                        AppConfig.Counters[Index].ImgUp = StringCopy(Value);
                    }
                    else if(StringsAreEqual(CounterSplit[2], L"imgdown"))
                    {
                        AppConfig.Counters[Index].ImgDown = StringCopy(Value);
                    }
                    else if(StringsAreEqual(CounterSplit[2], L"soundup"))
                    {
                        AppConfig.Counters[Index].SoundUp = StringCopy(Value);
                    }
                    else if(StringsAreEqual(CounterSplit[2], L"sounddown"))
                    {
                        AppConfig.Counters[Index].SoundDown = StringCopy(Value);
                    }
                    else if(StringsAreEqual(CounterSplit[2], L"soundmilestone"))
                    {
                        AppConfig.Counters[Index].SoundMilestone = StringCopy(Value);
                    }
                    else if(StringsAreEqual(CounterSplit[2], L"imghiddenup"))
                    {
                        AppConfig.Counters[Index].ImgHiddenUp = StringCopy(Value);
                    }
                    else if(StringsAreEqual(CounterSplit[2], L"imghiddendown"))
                    {
                        AppConfig.Counters[Index].ImgHiddenDown = StringCopy(Value);
                    }
                }
                else
                {
                    // NOTE: These have runtime default values, skip if value == 'default'
                    if(!StringsAreEqual(L"default", Value))
                    {
                        if(StringsAreEqual(L"erik_regular", Type))
                        {
                            AppConfig.ErikRegular = StringCopy(Value);
                        }
                        else if(StringsAreEqual(L"erik_smor", Type))
                        {
                            AppConfig.ErikSmor = StringCopy(Value);
                        }
                        else if(StringsAreEqual(L"erik_surprised", Type))
                        {
                            AppConfig.ErikSurprised = StringCopy(Value);
                        }
                        else if(StringsAreEqual(L"book_img", Type))
                        {
                            AppConfig.Book = StringCopy(Value);
                        }
                        else if(StringsAreEqual(L"angry_tom", Type))
                        {
                            AppConfig.AngryTom = StringCopy(Value);
                        }
                        else if(StringsAreEqual(L"updown_x", Type))
                        {
                            AppConfig.UpDownX = ParseValueInt(Value);
                        }
                        else if(StringsAreEqual(L"updown_y", Type))
                        {
                            AppConfig.UpDownY = ParseValueInt(Value);
                        }
                        else if(StringsAreEqual(L"updown_w", Type))
                        {
                            AppConfig.UpDownW = ParseValueInt(Value);
                        }
                        else if(StringsAreEqual(L"updown_h", Type))
                        {
                            AppConfig.UpDownH = ParseValueInt(Value);
                        }
                        else if(StringsAreEqual(L"date_x", Type))
                        {
                            AppConfig.DateX = ParseValueInt(Value);
                        }
                        else if(StringsAreEqual(L"date_y", Type))
                        {
                            AppConfig.DateY = ParseValueInt(Value);
                        }
                        else if(StringsAreEqual(L"date_w", Type))
                        {
                            AppConfig.DateW = ParseValueInt(Value);
                        }
                        else if(StringsAreEqual(L"date_h", Type))
                        {
                            AppConfig.DateH = ParseValueInt(Value);
                        }
                        else if(StringsAreEqual(L"default_image", Type))
                        {
                            AppConfig.DefaultImage = StringCopy(Value);
                        }
                        else if(StringsAreEqual(L"default_hidden", Type))
                        {
                            AppConfig.DefaultHiddenImage = StringCopy(Value);
                        }
                        else if(StringsAreEqual(L"timer_mod_ms", Type))
                        {
                            AppConfig.TimerModMS = ParseValueInt(Value);
                        }
                        else
                        {
                            FoundConfig = false;
                        }
                    }
                    if(FoundConfig)
                    {
                        continue;
                    }
                    // NOTE: These don't have runtime default values and thus care if
                    // value is 'default'
                    if(StringsAreEqual(L"window_w", Type))
                    {
                        AppConfig.WindowWidth = ParseValueInt(Value);
                    }
                    else if(StringsAreEqual(L"window_h", Type))
                    {
                        AppConfig.WindowHeight = ParseValueInt(Value);
                    }
                }
                Win32_Deallocate(Fields);
            }
            else
            {
            }
        }
        if(AppConfig.CounterCount < AppConfig.AllocatedCounterCount)
        {
            for(u32 CounterIndex = 0;
                CounterIndex < AppConfig.AllocatedCounterCount;
                ++CounterIndex)
            {
                //if(AppConfig.Counters[CounterIndex].Used != GlobalUseIndex)
                {
                    ZeroStruct(AppConfig.Counters[CounterIndex]);
                }
            }
        }
    }
    else
    {
        OutputDebugString(L"Failed to open config file\n");
    }
    return(AppConfig);
}

internal u32 
LoadStatistics()
{
    win32_read_file ReadFile = Win32_ReadEntireFile(L"stats.csv");
    if(ReadFile.Content)
    {
        if(GlobalStatsNew.Stats)
        {
            for(u32 StatIndex = 0;
                StatIndex < GlobalStatsNew.StatCount;
                ++StatIndex)
            {
                if(GlobalStatsNew.Stats[StatIndex].Fields)
                {
                    Win32_Deallocate(GlobalStatsNew.Stats[StatIndex].Label);
                    Win32_Deallocate(GlobalStatsNew.Stats[StatIndex].Fields);
                    GlobalStatsNew.Stats[StatIndex].FieldCount = 0;
                    GlobalStatsNew.Stats[StatIndex].Fields = 0;
                }
            }
            GlobalStatsNew.StatCount = 0;
            Win32_Deallocate(GlobalStatsNew.Stats);
            GlobalStatsNew.Stats = 0;
        }
        
        wchar_t *Buffer = 0;
        int BufferSize =MultiByteToWideChar(CP_UTF8,0,ReadFile.Content,ReadFile.Size,Buffer,0);
        Buffer = Win32_Allocate(sizeof(wchar_t) * (BufferSize + 1));
        MultiByteToWideChar(CP_UTF8,0,ReadFile.Content,ReadFile.Size,Buffer,BufferSize);
        wchar_t *FilePointer = Buffer;
        wchar_t *Line = ReadLine(&FilePointer);
        u32 SplitCount = 0;
        wchar_t **Split = SplitString(Line, ',', &SplitCount);
        GlobalStatsNew.StatCount = _wtoi(Split[0]);
        if(!GlobalStatsNew.FieldNames)
        {
            GlobalStatsNew.FieldNameCount = SplitCount - 1;
            GlobalStatsNew.FieldNames = Win32_Allocate(sizeof(wchar_t*) * GlobalStatsNew.FieldNameCount);
            for(u32 FieldNameIndex = 0;
                FieldNameIndex < GlobalStatsNew.FieldNameCount;
                ++FieldNameIndex)
            {
                GlobalStatsNew.FieldNames[FieldNameIndex] = StringCopy(Split[FieldNameIndex + 1]);
            }
        }
        else
        {
            for(u32 SearchIndex = 0;
                SearchIndex = SplitCount - 1;
                SearchIndex)
            {
                b32 FoundMatch = false;
                for(u32 FieldNameIndex = 0;
                    FieldNameIndex < GlobalStatsNew.FieldNameCount;
                    ++FieldNameIndex)
                {
                    if(StringsAreEqual(GlobalStatsNew.FieldNames[FieldNameIndex], Split[SearchIndex + 1]))
                    {
                        FoundMatch = true;
                        break;
                    }
                }
                if(!FoundMatch)
                {
                    u32 OldSize = sizeof(wchar_t*) * GlobalStatsNew.FieldNameCount;
                    u32 Index = GlobalStatsNew.FieldNameCount++;
                    u32 NewSize = sizeof(wchar_t*) * GlobalStatsNew.FieldNameCount;
                    GlobalStatsNew.FieldNames[Index] = StringCopy(Split[SearchIndex + 1]);
                }
            }
        }
        if(GlobalStatsNew.StatCount > 0)
        {
            GlobalStatsNew.Stats = Win32_Allocate(sizeof(stat_line) * GlobalStatsNew.StatCount);
            for(u32 StatIndex = 0;
                StatIndex < GlobalStatsNew.StatCount;
                ++StatIndex)
            {
                u32 FieldCount = 0;
                Line = ReadLine(&FilePointer);
                wchar_t **Fields = SplitString(Line, ',', &FieldCount);
                GlobalStatsNew.Stats[StatIndex].Label = StringCopy(Fields[0]);
                GlobalStatsNew.Stats[StatIndex].FieldCount = FieldCount - 1;
                GlobalStatsNew.Stats[StatIndex].Fields = Win32_Allocate(sizeof(u32) * GlobalStatsNew.Stats[StatIndex].FieldCount);
                Assert(GlobalStatsNew.Stats[StatIndex].FieldCount >= GlobalStatsNew.FieldNameCount);
                for(u32 FieldIndex = 0;
                    FieldIndex < GlobalStatsNew.Stats[StatIndex].FieldCount;
                    ++FieldIndex)
                {
                    GlobalStatsNew.Stats[StatIndex].Fields[FieldIndex] = _wtoi(Fields[FieldIndex + 1]);
                }
                Win32_Deallocate(Fields);
            }
        }
        Win32_Deallocate(Split);
    }
    return(0);
}

internal void
WriteBuffer(HANDLE FileHandle, wchar_t *Buffer)
{
    DWORD BytesWritten = 0;
    u8 *UTF8Buffer = 0;
    s32 UTF8BufferSize = WideCharToMultiByte(CP_UTF8,0,Buffer,StringLength(Buffer),
                                             UTF8Buffer,0,0,0);
    UTF8Buffer = Win32_Allocate(UTF8BufferSize);
    WideCharToMultiByte(CP_UTF8,0,Buffer,StringLength(Buffer),
                        UTF8Buffer,UTF8BufferSize,0,0);
    if(!WriteFile(FileHandle, UTF8Buffer, UTF8BufferSize, &BytesWritten,0) == BytesWritten)
    {
        Assert(!"Hmm?");
    }
}

internal void
SaveStatistics()
{
    HANDLE FileHandle = CreateFile(L"stats.csv", GENERIC_WRITE, 0, 0, CREATE_ALWAYS, 0, 0);
    // TODO: We can technically overrun this buffer, make a better way of handling this
    wchar_t Buffer[512];
    if(FileHandle != INVALID_HANDLE_VALUE)
    {
        wchar_t *BufferPointer = Buffer;
        BufferPointer += swprintf(Buffer, sizeof(Buffer) - (s64)BufferPointer, L"%d,", GlobalStatsNew.StatCount);
        for(u32 FieldNameIndex = 0;
            FieldNameIndex < GlobalStatsNew.FieldNameCount;
            ++FieldNameIndex)
        {
            wchar_t *Format = L"%s,";
            if(FieldNameIndex == GlobalStatsNew.FieldNameCount - 1)
            {
                Format = L"%s\n";
            }
            BufferPointer += swprintf(BufferPointer, sizeof(Buffer) - (s64)BufferPointer, Format, GlobalStatsNew.FieldNames[FieldNameIndex]);
        }
        for(u32 StatIndex = 0;
            StatIndex < GlobalStatsNew.StatCount;
            ++StatIndex)
        {
            stat_line *Stat = GlobalStatsNew.Stats + StatIndex;
            BufferPointer += swprintf(BufferPointer, sizeof(Buffer) - (s64)BufferPointer, L"%s,", Stat->Label);
            for(u32 FieldIndex = 0;
                FieldIndex < GlobalStatsNew.FieldNameCount;
                ++FieldIndex)
            {
                wchar_t *Format = L"%d,";
                u32 Value = 0;
                // TODO(Sander): Look up field on name
                if(FieldIndex < Stat->FieldCount)
                {
                    Value = Stat->Fields[FieldIndex];
                }
                if(FieldIndex == Stat->FieldCount - 1)
                {
                    Format = L"%d\n";
                }
                BufferPointer += swprintf(BufferPointer, sizeof(Buffer) - (s64)BufferPointer, Format, Value);
            }
        }
        swprintf(BufferPointer, sizeof(Buffer) - (s64)BufferPointer, L"\n");
        WriteBuffer(FileHandle, Buffer);
        CloseHandle(FileHandle);
    }
}

internal void
AddCounterToStats(counter *Counter)
{
    wchar_t Buffer[512];
    b32 FoundMatch = false;
    GetWindowText(Counter->Handles->Text, Buffer, ArrayCount(Buffer));
    for(u32 FieldNameIndex = 0;
        FieldNameIndex < GlobalStatsNew.FieldNameCount;
        ++FieldNameIndex)
    {
        if(StringsAreEqual(GlobalStatsNew.FieldNames[FieldNameIndex], Buffer))
        {
            FoundMatch = true;
            break;
        }
    }
    if(!FoundMatch)
    {
        u32 OldSize = sizeof(wchar_t*) * GlobalStatsNew.FieldNameCount;
        u32 NameIndex = GlobalStatsNew.FieldNameCount++;
        u32 NewSize = sizeof(wchar_t*) * GlobalStatsNew.FieldNameCount;
        GlobalStatsNew.FieldNames = Win32_Reallocate(GlobalStatsNew.FieldNames, OldSize, NewSize);
        GlobalStatsNew.FieldNames[NameIndex] = StringCopy(Buffer);
        OutputDebugString(L"Added counter to stats");
    }
}

internal b32
UpdateStatistics()
{
    b32 Result = true;
    wchar_t Buffer[512];
    GetWindowText(DateText,Buffer,ArrayCount(Buffer));
    if(StringContains(Buffer, ','))
    {
        MessageBox(0,L"Label cannot contain commas", L"Error updating statistics",
                   MB_OK|MB_ICONEXCLAMATION);
        Result = false;
    }
    else
    {
        s32 FoundMatch = -1;
        for(u32 StatIndex = 0;
            StatIndex < GlobalStatsNew.StatCount;
            ++StatIndex)
        {
            if(StringsAreEqual(Buffer, GlobalStatsNew.Stats[StatIndex].Label))
            {
                FoundMatch = StatIndex;
                break;
            }
        }
        if(FoundMatch == -1)
        {
            u32 OldSize = sizeof(stat_line) * (GlobalStatsNew.StatCount);
            FoundMatch = GlobalStatsNew.StatCount++;
            u32 NewSize = sizeof(stat_line) * (GlobalStatsNew.StatCount);
            GlobalStatsNew.Stats = Win32_Reallocate(GlobalStatsNew.Stats, OldSize, NewSize);
        }
        if(!GlobalStatsNew.Stats[FoundMatch].Label)
        {
            GlobalStatsNew.Stats[FoundMatch].Label = StringCopy(Buffer);
        }
        if(GlobalStatsNew.Stats[FoundMatch].FieldCount < GlobalStatsNew.FieldNameCount)
        {
            u32 OldSize = sizeof(u32) * GlobalStatsNew.Stats[FoundMatch].FieldCount;
            GlobalStatsNew.Stats[FoundMatch].FieldCount = GlobalStatsNew.FieldNameCount;
            u32 NewSize = sizeof(u32) * GlobalStatsNew.Stats[FoundMatch].FieldCount;
            GlobalStatsNew.Stats[FoundMatch].Fields = Win32_Reallocate(GlobalStatsNew.Stats[FoundMatch].Fields, OldSize, NewSize);
        }
        for(u32 FieldIndex = 0;
            FieldIndex < GlobalStatsNew.Stats[FoundMatch].FieldCount;
            ++FieldIndex)
        {
            if(!GlobalCounters[FieldIndex].Handles->Hidden)
            {
                GetWindowText(GlobalCounters[FieldIndex].Handles->Buddy,Buffer,ArrayCount(Buffer));
                GlobalStatsNew.Stats[FoundMatch].Fields[FieldIndex] = _wtoi(Buffer);
            }
            else
            {
                GlobalStatsNew.Stats[FoundMatch].Fields[FieldIndex] = 0;
            }
        }
    }
    return(Result);
}

internal void
PlayAudioClip(audio_clip *AudioClip)
{
    if(AudioClip->IsValid)
    {
        if(!IsPlayingSound)
        {
            if(IDirectSoundBuffer_SetCurrentPosition(AudioClip->Buffer, 0) == DS_OK)
            {
                HRESULT Error = IDirectSoundBuffer_Play(AudioClip->Buffer, 0, 0, 0);
                if(Error == DS_OK)
                {
                    Win32_FillSoundBuffer(AudioClip);
                    SetTimer(MainWindow,
                             ID_PLAY_SOUND, 
                             AudioClip->LengthInSeconds*1.1f,0);
                    IsPlayingSound = true;
                }
            }
        }
    }
}

internal void
RenderToBackBuffer()
{
    if(WaitForSingleObject(RenderCommands.Mutex, INFINITE) == WAIT_OBJECT_0)
    {
        RECT Rect = {0};
        GetWindowRect(MainWindow, &Rect);
        HDC DeviceContextScreen = GetDC(0);
        HDC DeviceContextSource = CreateCompatibleDC(DeviceContextScreen);
        HBITMAP OldBitmapHandle = SelectObject(DeviceContextSource, GlobalBackBuffer.BitmapHandle);
        
        FillRect(DeviceContextSource, &Rect, (HBRUSH) (COLOR_WINDOW+1));
        
        u8 *At = RenderCommands.Buffer;
        for(u32 CommandIndex = 0;
            CommandIndex < RenderCommands.Count;
            ++CommandIndex)
        {
            push_command *Command = (push_command*)At;
            switch(Command->Type)
            {
                case RenderPushBitmap:
                {
                    push_bitmap *BitmapCommand = (push_bitmap*)Command->Data;
                    Win32_DrawBitmap(BitmapCommand->Bitmap, 
                                     BitmapCommand->x, BitmapCommand->y, BitmapCommand->w, BitmapCommand->h, DeviceContextSource);
                }break;
            }
            At += Command->Size;
        }
        RenderCommands.At = RenderCommands.Buffer;
        RenderCommands.Count = 0;
        
        SelectObject(DeviceContextSource, OldBitmapHandle);
        DeleteDC(DeviceContextSource);
        ReleaseDC(0, DeviceContextSource);
        
        ReleaseMutex(RenderCommands.Mutex);
    }
    InvalidateRect(MainWindow, 0, TRUE);
    UpdateWindow(MainWindow);
}

inline void*
PushCommand(u32 Type)
{
    umm Size = sizeof(push_command);
    switch(Type)
    {
        case RenderPushBitmap:
        {
            Size += sizeof(push_bitmap);
        }break;
    }
    Assert(RenderCommands.At + Size < RenderCommands.End);
    RenderCommands.Count++;
    push_command *Command = (void*)RenderCommands.At;
    RenderCommands.At += Size;
    Command->Type = Type;
    Command->Size = Size;
    return(Command);
}

inline void
PushBitmapXYWH(win32_bitmap *Bmp, s32 x, s32 y, s32 w, s32 h, b32 Render)
{
    if(WaitForSingleObject(RenderCommands.Mutex, INFINITE) == WAIT_OBJECT_0)
    {
        push_command *Command = PushCommand(RenderPushBitmap);
        push_bitmap *BitmapCommand = (push_bitmap*)Command->Data;
        BitmapCommand->Bitmap = Bmp;
        BitmapCommand->x = x;
        BitmapCommand->y = y;
        BitmapCommand->w = w;
        BitmapCommand->h = h;
        ReleaseMutex(RenderCommands.Mutex);
    }
    
    if(Render)
    {
        RenderToBackBuffer();
    }
}

inline void
PushBitmap(win32_bitmap *Bmp, b32 Render)
{
    PushBitmapXYWH(Bmp, 0, 0, Bmp->Width, Bmp->Height, Render);
}

inline void
PushBitmapXY(win32_bitmap *Bmp, s32 x, s32 y, b32 Render)
{
    PushBitmapXYWH(Bmp, x, y, Bmp->Width, Bmp->Height, Render);
}

global win32_bitmap DancingTom[2];
global win32_bitmap DancingErik[2];

DWORD
DanceAnimProc(LPVOID lpParameter)
{
    for(u32 i = 0; i < GlobalCounterCount; ++i)
    {
        HideCounter(&GlobalCounters[i], true);
    }
    if(DancingTom[0].Pixels == 0)
    {
        DancingTom[0] = Win32_LoadBitmap(L"dancin_tom0.png");
        DancingTom[1] = Win32_LoadBitmap(L"dancin_tom1.png");
    }
    if(DancingErik[0].Pixels == 0)
    {
        DancingErik[0] = Win32_LoadBitmap(L"dancin_erik0.png");
        DancingErik[1] = Win32_LoadBitmap(L"dancin_erik1.png");
    }
    if(!GlobalDanceSound.IsValid)
    {
        GlobalDanceSound = LoadAudioClip(L"the_dance.ogg");
    }
    PlayAudioClip(&GlobalDanceSound);
    
    for(u32 i = 0; i < 10; ++i)
    {
        PushBitmap(&DancingErik[0], false);
        PushBitmapXY(&DancingTom[0], 70, 50, true);
        Sleep(500);
        PushBitmap(&DancingErik[1], false);
        PushBitmapXY(&DancingTom[1], 70, 50, true);
        Sleep(500);
    }
    for(u32 i = 0; i < GlobalCounterCount; ++i)
    {
        HideCounter(&GlobalCounters[i], false);
    }
    SetTimer(MainWindow,
             ID_TIMER, 
             1,0);
    return(0);
}

DWORD 
TestAnimateProc(LPVOID lpParameter)
{
    u32 Index = (u32)lpParameter;
    PlayAudioClip(&GlobalCounters[Index].SoundMilestone);
    for(u32 i = 0; i < 4; ++i)
    {
        PushBitmap(&GlobalCounters[Index].BitmapUp, true);
        Sleep(70);
        PushBitmap(&DefaultBitmap, true);
        Sleep(70);
    }
    PushBitmap(&GlobalCounters[Index].BitmapUp, true);
    SetTimer(MainWindow,
             ID_TIMER, 
             1000,0);
    return(0);
}

DWORD
TakeoverAnimProc(LPVOID lpParameter)
{
    for(u32 i = 0; i < GlobalCounterCount; ++i)
    {
        HideCounter(&GlobalCounters[i], true);
    }
    PushBitmap(&ErikSurprisedBitmap, true);
    Sleep(1700);
    PushBitmap(&BookBitmap, true);
    Sleep(1900);
    PushBitmap(&AngryTomBitmap, true);
    Sleep(2000);
    PushBitmap(&DefaultBitmap, true);
    Host = Tom;
    for(u32 i = 0; i < GlobalCounterCount; ++i)
    {
        HideCounter(&GlobalCounters[i], false);
    }
    SetTimer(MainWindow,
             ID_TIMER, 
             1,0);
    return(0);
}

internal win32_bitmap*
GetHostImage(s32 Dir, u32 CounterValue, u32 CounterIndex)
{
    win32_bitmap *Result = &DefaultBitmap;
    switch(Host)
    {
        case Tom:
        {
            if(Dir > 0)
            {
                if(CounterValue % 10 == 0)
                {
                    CreateThread(0, 0, TestAnimateProc, (void*)CounterIndex, 0, 0);
                }
                else
                {
                    PlayAudioClip(&GlobalCounters[CounterIndex].SoundUp);
                    if(GlobalHiddenMode && GlobalCounters[CounterIndex].BitmapHiddenUp.Name)
                    {
                        Result = &GlobalCounters[CounterIndex].BitmapHiddenUp;
                    }
                    else
                    {
                        Result = &GlobalCounters[CounterIndex].BitmapUp;
                    }
                }
            }
            else if(Dir < 0)
            {
                PlayAudioClip(&GlobalCounters[CounterIndex].SoundDown);
                if(GlobalHiddenMode && GlobalCounters[CounterIndex].BitmapHiddenDown.Name)
                {
                    Result = &GlobalCounters[CounterIndex].BitmapHiddenDown;
                }
                else
                {
                    Result = &GlobalCounters[CounterIndex].BitmapDown;
                }
            }
            else
            {
                if(GlobalHiddenMode)
                {
                    Result = &DefaultHiddenBitmap;
                }
                else
                {
                    Result = &DefaultBitmap;
                }
            }
        }break;
        case Erik:
        {
            if(Dir > 0)
            {
                Result = &ErikSmorBitmap;
            }
            else
            {
                Result = &ErikRegularBitmap;
            }
        }break;
        case Bjorn:
        {
            if(Dir > 0)
            {
                Result = &ErikSmorBitmap;
            }
            else if(Dir < 0)
            {
                Result = &ErikRegularBitmap;
            }
        }break;
        case Ivar:
        {
            if(Dir > 0)
            {
                Result = &ErikSmorBitmap;
            }
            else if(Dir < 0)
            {
                Result = &ErikRegularBitmap;
            }
        }break;
    }
    return(Result);
}

internal void
CounterEditChange(u32 CounterIndex)
{
    s32 CounterValue = GetCounterValue(CounterIndex);
    if(CounterValue > GlobalCounters[CounterIndex].PrevValue)
    {
        PushBitmap(GetHostImage(1, CounterValue, CounterIndex), true);
    }
    else if(CounterValue < GlobalCounters[CounterIndex].PrevValue)
    {
        PushBitmap(GetHostImage(-1, CounterValue, CounterIndex), true);
    }
#if 0
    if(!BitmapToDraw->Name)
    {
        if(GlobalHiddenMode)
        {
            BitmapToDraw = &DefaultHiddenBitmap;
        }
        else
        {
            BitmapToDraw = &DefaultBitmap;
        }
    }
    InvalidateRect(MainWindow, 0, TRUE);
    UpdateWindow(MainWindow);
#endif
    
    GlobalCounters[CounterIndex].PrevValue = CounterValue;
    SetTimer(MainWindow,
             ID_TIMER, 
             1680,0);
    UpdateStatistics();
    SaveStatistics();
}

LRESULT CALLBACK
Win32_MainWindowCallback(HWND WindowHandle, UINT Message,
                         WPARAM wParam, LPARAM lParam)
{
    local b32 Dragging = false;
    local POINT StoredMouseP = {0};
    switch(Message)
    {
        case WM_SIZE:
        {
            DWORD WindowWidth = LOWORD(lParam);
            DWORD WindowHeight = HIWORD(lParam);
            Win32_FreeBitmap(&GlobalBackBuffer);
            GlobalBackBuffer.Width = WindowWidth;
            GlobalBackBuffer.Height = WindowHeight;
            GlobalBackBuffer.BitmapHandle = Win32_CreateBitmap(GlobalBackBuffer.Width, 
                                                               GlobalBackBuffer.Height, 
                                                               &GlobalBackBuffer.Pixels, 32);
        }break;
        case WM_CLOSE:
        case WM_DESTROY:
        {
            if(UpdateStatistics())
            {
                SaveStatistics();
                ExitProcess(0);
            }
            else
            {
                return(0);
            }
        }break;
        case WM_RBUTTONDOWN:
        case WM_RBUTTONUP:
        {
            SetForegroundWindow(WindowHandle);
            
            POINT p = {0};
            
            p.x = GET_X_LPARAM(lParam); 
            p.y = GET_Y_LPARAM(lParam); 
            
            ClientToScreen(WindowHandle, &p); 
            
            TrackPopupMenu(ContextMenu,
                           TPM_LEFTALIGN|TPM_TOPALIGN|TPM_RIGHTBUTTON,
                           p.x,p.y,0,WindowHandle,0);
        }break;
        case WM_LBUTTONDOWN:
        {
            Dragging = true;
            
            StoredMouseP.x = GET_X_LPARAM(lParam); 
            StoredMouseP.y = GET_Y_LPARAM(lParam); 
            
        }break;
        case WM_LBUTTONUP:
        {
            Dragging = false;
        }break;
        case WM_MOUSEMOVE:
        {
            if(Dragging)
            {
                POINT Pos = {GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam)};;
                RECT WindowRect;
                GetClientRect(WindowHandle, &WindowRect);
                s32 WindowWidth = WindowRect.right - WindowRect.left;
                s32 WindowHeight = WindowRect.bottom - WindowRect.top;
                ClientToScreen(WindowHandle, &Pos);
                SetWindowPos(WindowHandle,0,
                             Pos.x - StoredMouseP.x,
                             Pos.y - StoredMouseP.y,
                             WindowWidth,
                             WindowHeight,
                             SWP_NOSIZE|SWP_NOZORDER);
            }
        }break;
        case WM_PAINT:
        {
#if 1
            PAINTSTRUCT PaintStruct;
            HDC DeviceContext = BeginPaint(WindowHandle, &PaintStruct);
            
            Win32_DrawBitmap(&GlobalBackBuffer, 0, 0, GlobalBackBuffer.Width, GlobalBackBuffer.Height, DeviceContext);
            
            EndPaint(WindowHandle, &PaintStruct);
#else
            PAINTSTRUCT PaintStruct;
            HDC DeviceContext = BeginPaint(WindowHandle, &PaintStruct);
            if(GlobalBackBuffer.BitmapHandle)
            {
                Assert(BitmapToDraw);
                {
                    HDC DeviceContextScreen = GetDC(0);
                    HDC DeviceContextSource = CreateCompatibleDC(DeviceContextScreen);
                    HBITMAP OldBitmapHandle = SelectObject(DeviceContextSource, GlobalBackBuffer.BitmapHandle);
                    FillRect(DeviceContextSource, &PaintStruct.rcPaint, (HBRUSH) (COLOR_WINDOW+1));
                    
                    Win32_DrawBitmap(BitmapToDraw, 0, 0, BitmapToDraw->Width, BitmapToDraw->Height, DeviceContextSource);
                    SelectObject(DeviceContextSource, OldBitmapHandle);
                    DeleteDC(DeviceContextSource);
                    ReleaseDC(0, DeviceContextSource);
                    Win32_DrawBitmap(&GlobalBackBuffer, 0, 0, GlobalBackBuffer.Width, GlobalBackBuffer.Height, DeviceContext);
                }
                
                
                Win32_DrawBitmap(&GlobalBackBuffer, 0, 0, GlobalBackBuffer.Width, GlobalBackBuffer.Height, DeviceContext);
                
                EndPaint(WindowHandle, &PaintStruct);
            }
#endif
        }break;
        case WM_CTLCOLORSTATIC:
        {
            HDC hdcStatic = (HDC)wParam;
            SetTextColor(hdcStatic, RGB(0,0,0));
            SetBkMode(hdcStatic, TRANSPARENT);
            return((LRESULT)GetStockObject(NULL_BRUSH));
        }break;
        case WM_TIMER:
        {
            switch (wParam)
            { 
                case ID_TIMER:
                {
                    OutputDebugString(L"Resetting\n");
                    PushBitmap(GetHostImage(0, 0, 0), true);
                    
                    KillTimer(MainWindow, ID_TIMER);
#if 0
                    InvalidateRect(MainWindow, 0, TRUE);
                    UpdateWindow(MainWindow);
#endif
                }break;
                case ID_PLAY_SOUND:
                {
                    IsPlayingSound = false;
                }break;
                case ID_TOM_TAKEOVER:
                {
                    CreateThread(0, 0, TakeoverAnimProc, 0, 0, 0);
                    KillTimer(MainWindow, ID_TOM_TAKEOVER);
                }break;
            }
        }break;
        case WM_COMMAND:
        {
            if((HIWORD(wParam) == EN_CHANGE))
            {
                DWORD CounterEditTextID = LOWORD(wParam);
                for(u32 CounterIndex = 0;
                    CounterIndex < GlobalCounterCount;
                    ++CounterIndex)
                {
                    if(CounterEditTextID == GlobalCounters[CounterIndex].ID)
                    {
                        CounterEditChange(CounterIndex);
                    }
                }
            }
            switch(LOWORD(wParam))
            {
                case ID_EXIT:
                {
                    PostMessage(WindowHandle, WM_CLOSE, 0, 0);
                }break;
                case ID_SAVE:
                {
                    UpdateStatistics();
                    SaveStatistics();
                }break;
                case ID_VER:
                {
                    MessageBox(MainWindow,VERSION,L"Current version",MB_OK);
                }break;
                case EN_CHANGE:
                {
                    OutputDebugString(L"yey\n");
                }break;
                case ID_HIDE:
                {
                    GlobalHiddenMode = !GlobalHiddenMode;
                    PushBitmap(GetHostImage(0,0,0), true);
                }break;
                case ID_DANCE:
                {
                    CreateThread(0,0,DanceAnimProc,0,0,0);
                }break;
            };
        }break;
        case WM_VSCROLL:
        {
            if(LOWORD(wParam) == SB_THUMBPOSITION)
            {
                //CounterEditChange((HWND)lParam);
            }
        }break;
    }
    return(DefWindowProc(WindowHandle, Message, wParam, lParam));
}

internal u64
Win32_GetLastWriteTime(wchar_t *FilePath)
{
    u64 Result = 0;
    FILETIME WriteTime = {0};
    HANDLE FileHandle = CreateFile(FilePath, 
                                   GENERIC_READ, 
                                   FILE_SHARE_READ|FILE_SHARE_WRITE,
                                   0, OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,0);
    if(FileHandle != INVALID_HANDLE_VALUE)
    {
        if(GetFileTime(FileHandle, 0, 0, &WriteTime))
        {
            ULARGE_INTEGER LargeInt = {0};
            LargeInt.LowPart = WriteTime.dwLowDateTime;
            LargeInt.HighPart = WriteTime.dwHighDateTime;
            Result = LargeInt.QuadPart;
        }
        CloseHandle(FileHandle);
    }
    return(Result);
}

internal void
UpdateCounter(counter *Counter, wchar_t *Label, s32 x, s32 y)
{
    Assert(Counter->Handles);
    RECT BuddyRect;
    GetClientRect(Counter->Handles->Buddy, &BuddyRect);
    SetWindowPos(Counter->Handles->UpDown,0,
                 x + (BuddyRect.right - BuddyRect.left), 
                 y + 20, 0, 0,
                 SWP_NOSIZE);
    SetWindowPos(Counter->Handles->Buddy, 0,
                 x, y + 20, 0, 0,
                 SWP_NOSIZE);
    if(Label)
    {
        SetWindowText(Counter->Handles->Text, Label);
        InvalidateRect(MainWindow, 0, TRUE);
        UpdateWindow(MainWindow);
    }
    SetWindowPos(Counter->Handles->Text, 0,
                 x, y, 0, 0,
                 SWP_NOSIZE);
}

internal void
ReapplyConfigs(app_config *Config)
{
    // TODO: What happens if images are in folders? It seems these makes it any load using the name and not the path, investiage...
    // NOTE: ^- How about you read the code dummy
    wchar_t *DefaultImageName = ExtractFileName(Config->DefaultImage);
    if(DefaultImageName)
    {
        Win32_FreeBitmap(&DefaultBitmap);
        DefaultBitmap = Win32_LoadBitmap(Config->DefaultImage);
    }
    wchar_t *DefaultHiddenImageName = ExtractFileName(Config->DefaultHiddenImage);
    if(DefaultHiddenImageName)
    {
        Win32_FreeBitmap(&DefaultHiddenBitmap);
        DefaultHiddenBitmap = Win32_LoadBitmap(Config->DefaultHiddenImage);
    }
    wchar_t *ErikImageName = ExtractFileName(Config->ErikRegular);
    if(ErikImageName)
    {
        Win32_FreeBitmap(&ErikRegularBitmap);
        ErikRegularBitmap = Win32_LoadBitmap(Config->ErikRegular);
    }
    wchar_t *ErikSmorImageName = ExtractFileName(Config->ErikSmor);
    if(ErikSmorImageName)
    {
        Win32_FreeBitmap(&ErikSmorBitmap);
        ErikSmorBitmap = Win32_LoadBitmap(Config->ErikSmor);
    }
    wchar_t *ErikSurprisedImageName = ExtractFileName(Config->ErikSurprised);
    if(ErikSurprisedImageName)
    {
        Win32_FreeBitmap(&ErikSurprisedBitmap);
        ErikSurprisedBitmap = Win32_LoadBitmap(Config->ErikSurprised);
    }
    wchar_t *BookImageName = ExtractFileName(Config->Book);
    if(BookImageName)
    {
        Win32_FreeBitmap(&BookBitmap);
        BookBitmap = Win32_LoadBitmap(Config->Book);
    }
    wchar_t *AngryTomImageName = ExtractFileName(Config->AngryTom);
    if(AngryTomImageName)
    {
        Win32_FreeBitmap(&AngryTomBitmap);
        AngryTomBitmap = Win32_LoadBitmap(Config->AngryTom);
    }
    
    s32 WindowWidth = (Config->WindowWidth == DEFAULT_VALUE) ? (DefaultBitmap.Width == 0) ? 200 : DefaultBitmap.Width : Config->WindowWidth;
    s32 WindowHeight = (Config->WindowHeight == DEFAULT_VALUE) ? (DefaultBitmap.Height == 0) ? 200 : DefaultBitmap.Height : Config->WindowHeight;
    SetWindowPos(MainWindow,
                 0,0,0,
                 WindowWidth,
                 WindowHeight,
                 SWP_NOMOVE);
    MoveWindow(DateText, 
               Config->DateX, 
               Config->DateY, 
               Config->DateW, 
               Config->DateH, 
               TRUE);
    for(u32 CounterIndex = 0;
        CounterIndex < GlobalCounterCount;
        ++CounterIndex)
    {
        GlobalCounters[CounterIndex].Used = false;
    }
    for(u32 SearchIndex = 0;
        SearchIndex < Config->CounterCount;
        ++SearchIndex)
    {
        s32 FoundIndex = -1;
        for(u32 CounterIndex = 0;
            CounterIndex < GlobalCounterCount;
            ++CounterIndex)
        {
            if(GlobalCounters[CounterIndex].ID == Config->Counters[SearchIndex].ID)
            {
                FoundIndex = CounterIndex;
            }
        }
        if(FoundIndex == -1)
        {
            u32 OldSize = sizeof(counter) * GlobalCounterCount;
            GlobalCounterCount++;
            u32 NewSize = sizeof(counter) * GlobalCounterCount;
            GlobalCounters = Win32_Reallocate(GlobalCounters, OldSize, NewSize);
            FoundIndex = GlobalCounterCount - 1;
            if(!GlobalCounters[FoundIndex].Handles)
            {
                GlobalCounters[FoundIndex] = CreateCounter(Config->Counters[SearchIndex].Label, 
                                                           Config->Counters[SearchIndex].ImgUp,
                                                           Config->Counters[SearchIndex].ImgDown,
                                                           Config->Counters[SearchIndex].ImgHiddenUp,
                                                           Config->Counters[SearchIndex].ImgHiddenDown,
                                                           Config->Counters[SearchIndex].SoundUp,
                                                           Config->Counters[SearchIndex].SoundDown,
                                                           Config->Counters[SearchIndex].SoundMilestone,
                                                           Config->Counters[SearchIndex].x, 
                                                           Config->Counters[SearchIndex].y, 
                                                           100, 20, 0, 100, MainWindow,
                                                           0, Config->Counters[SearchIndex].ID);
            }
        }
        UpdateCounter(&GlobalCounters[FoundIndex], 
                      Config->Counters[SearchIndex].Label, 
                      Config->Counters[SearchIndex].x, 
                      Config->Counters[SearchIndex].y);
        AddCounterToStats(&GlobalCounters[FoundIndex]);
        GlobalCounters[FoundIndex].Used = true;
        HideCounter(&GlobalCounters[FoundIndex], false);
    }
    for(u32 CounterIndex = 0;
        CounterIndex < GlobalCounterCount;
        ++CounterIndex)
    {
        if(GlobalCounters[CounterIndex].Used == false)
        {
            HideCounter(&GlobalCounters[CounterIndex], true);
        }
    }
    
    local b32 TakeoverTimeInProgress = false;
    
    if(Host != Tom && !TakeoverTimeInProgress)
    {
        PushBitmap(GetHostImage(0,0,0), true);
        u32 MS = rand()%Config->TimerModMS;
        SetTimer(MainWindow,
                 ID_TOM_TAKEOVER, 
                 MS,0);
        TakeoverTimeInProgress = true;
        wchar_t b[100];
        swprintf(b, ArrayCount(b), L"%d \n", MS);
        OutputDebugString(b);
    }
#if 0
    InvalidateRect(MainWindow, 0, TRUE);
    UpdateWindow(MainWindow);
#endif
}

internal void
FreeConfig(app_config *Config)
{
    Win32_Deallocate(Config->DefaultImage);
    Win32_Deallocate(Config->Counters);
    // TODO: Free counters????
}

internal void
ReloadConfigFile(wchar_t *Filepath)
{
    app_config Config = ReadConfig(L"config.cfg");
    ReapplyConfigs(&Config);
    OutputDebugString(L"Reloading!\n");
    FreeConfig(&Config);
}

internal wchar_t*
GetExeDirectory()
{
    u32 BufferCount = 33000;
    wchar_t *Buffer = Win32_Allocate(sizeof(wchar_t) * BufferCount);
    DWORD l = GetModuleFileName(0,Buffer,BufferCount);
    
    wchar_t *LastSlash = Buffer;
    wchar_t *Scan = Buffer;
    do
    {
        if(*Scan == '\\' || 
           *Scan == '/')
        {
            LastSlash = Scan;
        }
    }while(*Scan++);
    LastSlash++;
    *LastSlash = 0;
    u32 DirLen = (((s64)LastSlash - (s64)Buffer) / sizeof(wchar_t)) + 1;
    
    wchar_t *Result = StringCopyN(Buffer, DirLen);
    Win32_Deallocate(Buffer);
    return(Result);
}

DWORD WINAPI
FileChangeProc(void *lpParameter)
{
    DWORD Len = GetCurrentDirectory(0,0);
    wchar_t *Buff = Win32_Allocate(sizeof(wchar_t) * Len);
    GetCurrentDirectory(Len, Buff);
    
    HANDLE ChangeHandle = FindFirstChangeNotification(Buff,FALSE,
                                                      FILE_NOTIFY_CHANGE_LAST_WRITE);
    
    if(ChangeHandle != INVALID_HANDLE_VALUE)
    {
        while(1)
        {
            if(WaitForSingleObject(ChangeHandle, INFINITE) == WAIT_OBJECT_0)
            {
                u64 FileSize = Win32_GetFileSize(L"config.cfg");
                if(FileSize)
                {
                    u64 WriteTime = Win32_GetLastWriteTime(L"config.cfg");
                    if(WriteTime - LastWriteTime > 100000)
                    {
                        ReloadConfigFile(L"config.cfg");
                        LastWriteTime = WriteTime;
                    }
                }
                else
                {
                    OutputDebugString(L"Filesize is zero, trying again!\n");
                }
                FindNextChangeNotification(ChangeHandle);
            }
        }
        FindCloseChangeNotification(ChangeHandle);
    }
    else
    {
        OutputDebugString(L"Hot reloading data will not work since the data directory is not valid!\n");
    }
    return(0);
}

int CALLBACK
WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR pCmdLine, int nCmdShow)
{
    RenderCommands.Mutex = CreateMutex(0,0,0);
    RenderCommands.Count = 0;
    umm RenderCommandsSize = Kilobytes(10);
    RenderCommands.At = RenderCommands.Buffer = Win32_Allocate(RenderCommandsSize);
    RenderCommands.End = RenderCommands.At + RenderCommandsSize;
    SYSTEMTIME SystemTime = {0};
    FILETIME FileTime = {0};
    GetSystemTime(&SystemTime);
    SystemTimeToFileTime(&SystemTime, &FileTime);
    srand(FileTime.dwLowDateTime);
#define RandMax 10000
    u32 Rand = rand() % RandMax;
    if(Rand >= 0 && Rand < RandMax/2)
    {
        Host = Tom;
    }
    else
    {
        Host = 1 + (Rand-(RandMax/2)) / ((RandMax/2)/3);
    }
    // TODO: REMOVE
    // TODO: REMOVE
    // TODO: REMOVE
    // TODO: REMOVE
    // TODO: REMOVE
    if(Host > 1) Host = 1;
#if 0
    wchar_t b[100];
    swprintf(b, ArrayCount(b), L"%d ", Rand);
    OutputDebugString(b);
    switch(Host)
    {
        case Tom:
        OutputDebugString(L"Host is Tom\n");
        break;
        case Erik:
        OutputDebugString(L"Host is Erik\n");
        break;
        case Bjorn:
        OutputDebugString(L"Host is Bjorn\n");
        break;
        case Ivar:
        OutputDebugString(L"Host is Ivar\n");
        break;
        default:
        OutputDebugString(L"NEI NEI NEI\n");
    }
#endif
    
    CreateThread(0, 0, FileChangeProc, 0, 0, 0);
    
    LoadStatistics();
    
    WNDCLASSEX WindowClass = {0};
    WindowClass.cbSize = sizeof(WindowClass);
    WindowClass.style = CS_VREDRAW | CS_HREDRAW;
    WindowClass.lpfnWndProc = Win32_MainWindowCallback;
    WindowClass.hInstance = hInstance;
    WindowClass.lpszClassName = L"Godt";
    WindowClass.hIcon = LoadIcon(0, IDI_APPLICATION);
    WindowClass.hCursor = LoadCursor(0, IDC_ARROW);
    WindowClass.hIconSm = LoadIcon(NULL, IDI_WINLOGO);
    if(RegisterClassExW(&WindowClass))
    {
        DWORD ExtendedStyle = WS_EX_LAYERED|WS_EX_TOPMOST;
        DWORD WindowStyle = WS_POPUP;
        MainWindow = CreateWindowEx(ExtendedStyle,
                                    WindowClass.lpszClassName,
                                    WindowClass.lpszClassName,
                                    WindowStyle,
                                    CW_USEDEFAULT, CW_USEDEFAULT, 
                                    240, 0,
                                    0, 0, hInstance, 0);
        if(MainWindow)
        {
            Win32_InitDSound(MainWindow, DSoundBufferSamplesPerSecond);
            TopMenu = CreateMenu();
            ContextMenu = CreateMenu();
            AppendMenu(ContextMenu, MF_STRING, 0, L"New Day");
            AppendMenu(ContextMenu, MF_STRING, 0, L"Statistics");
            AppendMenu(ContextMenu, MF_STRING, ID_SAVE, L"Save");
            AppendMenu(ContextMenu, MF_STRING, ID_HIDE, L"Hide");
            AppendMenu(ContextMenu, MF_STRING, ID_DANCE, L"Dance");
            AppendMenu(ContextMenu, MF_SEPARATOR, 0, 0);
            AppendMenu(ContextMenu, MF_STRING, ID_VER, L"Version");
            AppendMenu(ContextMenu, MF_SEPARATOR, 0, 0);
            AppendMenu(ContextMenu, MF_STRING, ID_EXIT, L"Exit");
            AppendMenu(TopMenu, MF_POPUP, (UINT_PTR)ContextMenu, L"");
            
            if(MainWindow)
            {
                SetLayeredWindowAttributes(MainWindow,RGB(0,255,0),255,LWA_COLORKEY);
                
                INITCOMMONCONTROLSEX icex;
                
                icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
                icex.dwICC  = ICC_UPDOWN_CLASS;
                InitCommonControlsEx(&icex); 
                
                DateText = CreateWindowEx(0,
                                          WC_EDIT,
                                          WindowClass.lpszClassName,
                                          WS_CHILD|WS_VISIBLE,
                                          0,0,0,0,
                                          MainWindow,
                                          0,hInstance,0);
                
                wchar_t Buffer[100];
                SYSTEMTIME SystemTime = {0};
                GetSystemTime(&SystemTime);
                swprintf(Buffer, sizeof(Buffer), L"%02d.%02d.%d", 
                         SystemTime.wDay,
                         SystemTime.wMonth,
                         SystemTime.wYear);
                SetWindowText(DateText, Buffer);
                /*
                Counter1 = CreateCounter(0, 0, 0, 100, 20, 0, 100, MainWindow, hInstance);
                */
                ShowWindow(MainWindow, SW_SHOW);
                
                ReloadConfigFile(L"config.cfg");
                
                GlobalGodtSound = LoadAudioClip(L"godt.ogg");
                GlobalSadSound = LoadAudioClip(L"sad.ogg");
                
                PushBitmap(GetHostImage(0,0,0), true);
                
                MSG Message;
                BOOL GetMessageResult;
                while((GetMessageResult = GetMessage(&Message, 0, 0, 0)) != 0)
                {
                    if(GetMessageResult >= 0)
                    {
                        TranslateMessage(&Message);
                        DispatchMessage(&Message);
                    }
                    else
                    {
                        Assert(!"ERROR");
                    }
                }
            }
        }
    }
}

#include "stb_vorbis.c"
