#ifndef _SP_DEFINES_H
#define _SP_DEFINES_H
#ifndef internal
#define internal static
#endif
#ifndef global
#define global static
#endif
#ifndef local
#define local static
#endif

#ifndef true
#define true     1
#endif
#ifndef false
#define false    0
#endif

#define ArrayCount(Array) (sizeof(Array) / sizeof(Array[0]))
#define ArrayIndex(Element, ArrayStart) ((Element - ArrayStart))

#define ZeroStruct(Struct) (memset(&Struct, 0, sizeof(Struct)))
#define ZeroPointer(Pointer) (memset(Pointer, 0, sizeof(*Pointer)))

#define Min(A, B) ((A < B) ? (A) : (B))
#define Max(A, B) ((A > B) ? (A) : (B))
#define Clamp(x,xmin,xmax)  ((x) < (xmin) ? (xmin) : (x) > (xmax) ? (xmax) : (x))

#define Abs(Value) (Value < 0) ? -(Value) : (Value)
#define Square(Value) (Abs(Value*Value))

#define Kilobytes(Value) ((Value)*1024LL)
#define Megabytes(Value) (Kilobytes(Value)*1024LL)
#define Gigabytes(Value) (Megabytes(Value)*1024LL)
#define Terabytes(Value) (Gigabytes(Value)*1024LL)

#define Assert(Expression) if(!(Expression)) { *(int *)0 =0; }

#define InvalidDefaultCase default: Assert(0)

#define Pi32 3.14159265358979323846f
#define Tau32 2*Pi32
#define DegToRad(Angle) ((Angle)*(Pi32/180.0f))
#define RadToDeg(Angle) ((Angle)*(180.0f/Pi32))

#endif